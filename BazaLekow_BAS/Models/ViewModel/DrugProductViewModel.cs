﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BazaLekow_BAS.Models.ViewModel
{
    public class DrugProductViewModel
    {
        public DrugProductViewModel()
        {

        }
        public DrugProductViewModel(DrugProduct dp)
        {
            Id = dp.id;
            Drug_id = dp.drug_id;
            Shape = dp.shape;
            Dose = dp.dose;
            Package = dp.package;
            Manufacturer = dp.manufacturer;
            Full_price = dp.full_price;
            Refunded_price = dp.refunded_price;
        }
        public DrugProduct ToDrugProduct()
        {
            DrugProduct dp = new DrugProduct();

            dp.id = Id;
            dp.drug_id = Drug_id;
            dp.shape = Shape;
            dp.dose = Dose;
            dp.package = Package;
            dp.manufacturer = Manufacturer;
            dp.full_price = Full_price;
            dp.refunded_price = Refunded_price;

            return dp;
        }

        public int  Id { get; set; }
        public int Drug_id { get; set; }
        public string Shape { get; set; }
        public string  Dose { get; set; }
        public string Package { get; set; }
        public string Manufacturer{ get; set; }
        public string Full_price { get; set; }
        public string Refunded_price { get; set; }
    }
}