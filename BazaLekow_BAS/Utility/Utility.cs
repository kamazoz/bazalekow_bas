﻿using BazaLekow_BAS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace BazaLekow_BAS.Utility
{
    static public class Utility
    {
       
        static public Func<string, byte[]> hashPassword = (pwStr) => {
            using (SHA512CryptoServiceProvider provider = new SHA512CryptoServiceProvider())
            {
                return provider.ComputeHash(Encoding.UTF8.GetBytes(pwStr));
            }
        };

        static public Boolean compareHashPasswords(byte[] b1, byte[] b2)
        {
            if (b1.Length != b2.Length)
                return false;

            for(int i = 0; i < b1.Length; ++i)
            {
                if (b1[i] != b2[i])
                    return false;
            }

            return true;
        }
    }
}