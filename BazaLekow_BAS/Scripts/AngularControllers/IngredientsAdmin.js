﻿angular.module('MyApp')
.controller('IngredientsAdminController', ['$window', '$rootScope', '$scope', 'IngredientAdminService', function ($window, $rootScope, $scope, IngredientAdminService) {
    var vm = this;

    applyNewModel = function () {
        IngredientAdminService.getIngredients().then(function (response) {
            vm.model = response.data;
        });
    }

    $scope.resize = function ($event) {
        $event.target.style.height = $event.target.scrollHeight + "px";
    };

    $scope.detailsIngredient = function () {
        $scope.isEdit = true;
        IngredientAdminService.detailsIngredient(this.d).then(function (d) {
            $scope.Ing = d.data;
            $scope.Ing.Id = d.data.Id;
        });
        $('#viewModal').modal('show');
    }

    $scope.addIngredient = function () {
        IngredientAdminService.addIngredient($scope.Ing).then(function (d) {
            applyNewModel();
        });
        $('#viewModal').modal('hide');
    }

    $scope.deleteIngredient = function () {
        IngredientAdminService.deleteIngredient(this.d).then(function (d) {
            applyNewModel();
        });
    }

    $scope.editIngredient = function () {
        IngredientAdminService.editIngredient($scope.Ing).then(function (d) {
            applyNewModel();
        });
        $('#viewModal').modal('hide');
    }

    $scope.openModal = function () {
        $scope.Ing = {};
        $scope.Ing.Data = {};
        $scope.isEdit = false;
        $('#viewModal').modal('show');
    }

    $scope.closeModal = function () {
        $('#viewModal').modal('hide');
    }

    _init(window.MvcModel);

    function _init(model) {
        vm.model = model;

    }
}])
.factory('IngredientAdminService', ['$http', function ($http) {
    var IngredientAdminService = {};
    IngredientAdminService.getIngredients = function () {
        return $http.get('/Ingredients/IndexJSON');
    };

    IngredientAdminService.addIngredient = function (ingredient) {
        return $http.post('/Ingredients/CreateJSON', 
            {   'id': ingredient.Id, 
                'name': ingredient.Name,
                'effects': ingredient.Data.Effects,
                'indications': ingredient.Data.Indications,
                'contradictions': ingredient.Data.Contradictions,
                'interactions': ingredient.Data.Interactions,
                'side_effects': ingredient.Data.Side_effects,
                'pregnancy': ingredient.Data.Pregnancy,
                'dosage': ingredient.Data.Dosage,
                'comments': ingredient.Data.Comments,
                'other': ingredient.Data.Other
            }
            );
    };

    IngredientAdminService.deleteIngredient = function (ingredient) {
        return $http.post('/Ingredients/DeleteJSON', ingredient, { params: { 'id': ingredient.Id } });
    };

    IngredientAdminService.detailsIngredient = function (ingredient) {
        return $http.get('/Ingredients/DetailsGuestJSON', { params: { 'id': ingredient.Id } });
    };

    IngredientAdminService.editIngredient = function (ingredient) {
        return $http.post('/Ingredients/EditJSON',
            {
                'id': ingredient.Id,
                'name': ingredient.Name,
                'effects': ingredient.Data.Effects,
                'indications': ingredient.Data.Indications,
                'contradictions': ingredient.Data.Contradictions,
                'interactions': ingredient.Data.Interactions,
                'side_effects': ingredient.Data.Side_effects,
                'pregnancy': ingredient.Data.Pregnancy,
                'dosage': ingredient.Data.Dosage,
                'comments': ingredient.Data.Comments,
                'other': ingredient.Data.Other
            }
            );
    };
    return IngredientAdminService;
}]);
