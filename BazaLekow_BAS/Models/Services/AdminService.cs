﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BazaLekow_BAS.Models.Services
{
    public class AdminService : IAdminService
    {
        private IBLDbContext db;

        public AdminService(IBLDbContext db)
        {
            this.db = db;
        }

        public bool Add(Admin item)
        {
            db.Admins.Add(item);
            db.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            db.Admins.Remove(db.Admins.First(d => d.id == id));
            db.SaveChanges();
            return true;
        }

        public Admin Get(int id)
        {
            return db.Admins.FirstOrDefault(d => d.id == id);
        }

        public IQueryable<Admin> GetAll()
        {
            return db.Admins;
        }

        public bool Update(Admin item)
        {
            db.Entry(item).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return true;
        }
    }
}
