﻿using BazaLekow_BAS.Models;
using BazaLekow_BAS.Models.Services;
using BazaLekow_BAS.Models.ViewModel;
using BazaLekow_BAS.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace BazaLekow_BAS.Controllers
{
    public class LoginController : Controller
    {
        private BLEntities db = new BLEntities();

        private IAdminService service;

        public LoginController(IAdminService service)
        {
            this.service = service;
        }

        [HttpPost]
        [ValidateAntiForgeryHeader]
        public JsonResult Index(AdminViewModel model)
        {
            var s = service.GetAll().ToList();

            var user = s.Where(b => b.username.Equals(model.Username)).FirstOrDefault() as Admin;

            if (user != null)
            {
                if(Utility.Utility.compareHashPasswords(user.password, Utility.Utility.hashPassword(model.Password)))
                {
                    Session["isAuthorized"] = true;
                    Session["username"] = user.username;

                    return Json(new { username = user.username }, JsonRequestBehavior.AllowGet);
                }
            }
            

            return Json( new { error = "Username or password is wrong" } , JsonRequestBehavior.AllowGet);
        }

        public JsonResult Logout()
        {
            Session["isAuthorized"] = null;
            Session["username"] = null;
            return Json(new { info = "Wylogowano" }, JsonRequestBehavior.AllowGet);
        }
    }
}