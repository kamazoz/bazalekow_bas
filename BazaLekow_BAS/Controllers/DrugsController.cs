﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BazaLekow_BAS.Models;
using BazaLekow_BAS.Utility;
using BazaLekow_BAS.Models.Services;
using BazaLekow_BAS.Models.ViewModel;

namespace BazaLekow_BAS.Controllers
{
    [AuthorizationFilter]
    public class DrugsController : Controller
    {
        private IDrugService service;

        public DrugsController(IDrugService service)
        {
            this.service = service;
        }

        // GET: Drugs
        public ActionResult Index()
        {
            return View(service.GetSmallDrugs().ToList());
        }

        // GET: Drugs Json
        public JsonResult IndexJSON()
        {
            return Json(service.GetSmallDrugs().ToList(), JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        // GET: Drugs Guest
        public ActionResult Guest()
        {
            return View(service.GetSmallDrugs().ToList());

        }

        [AllowAnonymous]
        // GET: Drugs Guest Json
        public JsonResult GuestJSON()
        {
            return Json(service.GetSmallDrugs().ToList(), JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        // GET: Drugs/Details/id
        public JsonResult DetailsGuestJSON(int? id)
        {
            if(id == null)
            {
                return Json("Nieznana wartość", JsonRequestBehavior.AllowGet);
            }

            return Json(service.GetWholeData(id.Value), JsonRequestBehavior.AllowGet);
        }

        
        [HttpPost]
        [ValidateAntiForgeryHeader]
        //public JsonResult CreateJSON(DrugSmallViewModel drug, DrugDataViewModel drugData, DrugProductViewModel[] products, [Bind(Include = "Ingredients")] IngredientSmallViewModel[] ingredients)
        public JsonResult CreateJSON(DrugWholeDataSetViewModel drug)
        {
            if(drug == null || drug.Name == null)
            {
                return Json("Błąd", JsonRequestBehavior.AllowGet);
            }
            if (ModelState.IsValid)
            {
                service.Add(drug);
                return Json("Success", JsonRequestBehavior.AllowGet);
            }

            return Json("Błąd", JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [ValidateAntiForgeryHeader]
        public ActionResult EditJSON(DrugWholeDataSetViewModel drug)
        {
            if (drug == null || drug.Name == null)
            {
                return Json("Błąd", JsonRequestBehavior.AllowGet);
            }
            if (ModelState.IsValid)
            {
                service.Update(drug);
                return Json("Success", JsonRequestBehavior.AllowGet);
            }

            return Json("Błąd", JsonRequestBehavior.AllowGet);
        }

        // POST: Drugs/Delete/5
        [HttpPost]
        [ValidateAntiForgeryHeader]
        public ActionResult DeleteJSON(int? id)
        {
            if(id == null)
                return Json("Błąd", JsonRequestBehavior.AllowGet);

            service.Delete(id.Value);
            return Json("Success", JsonRequestBehavior.AllowGet);
        }
    }
}
