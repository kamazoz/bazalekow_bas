﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BazaLekow_BAS.Models;
using BazaLekow_BAS.Utility;
using BazaLekow_BAS.Models.Services;
using BazaLekow_BAS.Models.ViewModel;

namespace BazaLekow_BAS.Controllers
{
    [AuthorizationFilter]

    public class IngredientsController : Controller
    {
        private IIngredientService service;

        public IngredientsController(IIngredientService service)
        {
            this.service = service;
        }

        // GET: Ingredients
        public ActionResult Index()
        {
            return View(service.GetSmallIngredients().ToList());
        }

        public JsonResult IndexJSON()
        {
            return Json(service.GetSmallIngredients().ToList(), JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        // GET: Ingredients Guest
        public ActionResult Guest()
        {
            return View(service.GetSmallIngredients().ToList());
        }

        [AllowAnonymous]
        // GET: Ingredients Guest
        public JsonResult GuestJSON()
        {
            return Json(service.GetSmallIngredients().ToList(), JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        // GET: Ingredients/Details/5
        public JsonResult DetailsGuestJSON(int? id)
        {
            if (id == null)
            {
                return Json("Złe id", JsonRequestBehavior.AllowGet);
            }

            return Json(service.GetWholeData(id.Value), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [ValidateAntiForgeryHeader]

        public JsonResult CreateJSON([Bind(Include = "id,name")] IngredientSmallViewModel ingredient,[Bind(Include = "effects,indications,contradictions,interactions,side_effects,pregnancy,dosage,comments,other")] IngredientDataViewModel data)
        {
            if (ingredient == null || data == null)
                return Json("Błąd", JsonRequestBehavior.AllowGet);
            if (ModelState.IsValid)
            {
                service.Add(ingredient.ToIngredient(), data.ToIngredientData());
                return Json("Błąd", JsonRequestBehavior.AllowGet);
            }

            return Json("Błąd", JsonRequestBehavior.AllowGet);
        }

        
        [HttpPost]
        [ValidateAntiForgeryHeader]
        public JsonResult EditJSON([Bind(Include = "id,name")] IngredientSmallViewModel ingredient, [Bind(Include = "effects,indications,contradictions,interactions,side_effects,pregnancy,dosage,comments,other")] IngredientDataViewModel data)
        {
            if (ingredient == null || data == null)
                return Json("Błąd", JsonRequestBehavior.AllowGet);

            if (ModelState.IsValid)
            {
                var oldIngredient = service.Get(ingredient.Id);
                var oldData = service.GetData(oldIngredient.data_id);
                oldIngredient.name = ingredient.Name;
                oldData.comments = data.Comments;
                oldData.contradictions = data.Contradictions;
                oldData.dosage = data.Dosage;
                oldData.effects = data.Effects;
                oldData.indications = data.Indications;
                oldData.interactions = data.Interactions;
                oldData.other = data.Other;
                oldData.pregnancy = data.Pregnancy;
                oldData.side_effects = data.Side_effects;

                service.Update(oldIngredient, oldData);
                return Json("Success", JsonRequestBehavior.AllowGet);
            }

            return Json("Błąd", JsonRequestBehavior.AllowGet);
        }
        

        
        // POST: Ingredients/Delete/5
        [HttpPost]
        [ValidateAntiForgeryHeader]
        public JsonResult DeleteJSON(int? id)
        {
            if(id == null)
                return Json("Błąd", JsonRequestBehavior.AllowGet);

            service.Delete(id.Value);
            return Json("Success", JsonRequestBehavior.AllowGet);
        }
    }
}
