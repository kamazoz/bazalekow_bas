﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BazaLekow_BAS.Models.ViewModel
{
    public class DrugSmallViewModel
    {
        public DrugSmallViewModel()
        {

        }
        public DrugSmallViewModel(Drug d)
        {
            Id = d.id;
            Name = d.name;
        }

        public DrugSmallViewModel(Drug d, string description) : this(d)
        {
            Description = description;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}