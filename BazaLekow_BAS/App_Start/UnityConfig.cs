using BazaLekow_BAS.Models;
using BazaLekow_BAS.Models.Services;
using System.Web.Mvc;
using Unity;
using Unity.Lifetime;
using Unity.Mvc5;

namespace BazaLekow_BAS
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();
            
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
            container.RegisterType<IBLDbContext, BLEntities>(new ContainerControlledLifetimeManager());
            container.RegisterType<IDrugService, DrugService>(new ContainerControlledLifetimeManager());
            container.RegisterType<IIngredientService, IngredientService>(new ContainerControlledLifetimeManager());
            container.RegisterType<IAdminService, AdminService>(new ContainerControlledLifetimeManager());
        }
        
    }
}