﻿using BazaLekow_BAS.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BazaLekow_BAS.Models.Services
{
    public interface IDrugService : IService<Drug>
    {
        DrugViewModel GetWholeData(int id);
        IEnumerable<DrugSmallViewModel> GetSmallDrugs();
        bool Add(DrugWholeDataSetViewModel drug);
        bool Update(DrugWholeDataSetViewModel drug);
    }
}