﻿using BazaLekow_BAS.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BazaLekow_BAS.Models.Services
{
    public class IngredientService : IIngredientService
    {
        private IBLDbContext db;

        public IngredientService(IBLDbContext db)
        {
            this.db = db;
        }

        public bool Add(Ingredient item)
        {
            db.Ingredients.Add(item);
            db.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            var Ing = db.Ingredients.First(d => d.id == id);
            db.IngredientDatas.Remove(db.IngredientDatas.First(ing => ing.id == Ing.data_id));

            foreach (var delete in db.Drug_Ingredient.Where(di => di.ingredient_id == Ing.id).ToArray() ?? new Drug_Ingredient[0])
            {
                db.Entry(delete.Drug).State = System.Data.Entity.EntityState.Modified;
                db.Drug_Ingredient.Remove(delete);
            }

            db.Ingredients.Remove(Ing);

            db.SaveChanges();
            return true;
        }

        public Ingredient Get(int id)
        {
            return db.Ingredients.FirstOrDefault(d => d.id == id);
        }

        public IQueryable<Ingredient> GetAll()
        {
            return db.Ingredients;
        }

        public bool Update(Ingredient item)
        {
            db.Entry(item).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return true;
        }

        public IEnumerable<IngredientViewModel> GetAllWithDrugs()
        {

            var ingredients = db.Ingredients;
            List<IngredientViewModel> da = new List<IngredientViewModel>();
            foreach (var d in ingredients)
            {
                var data = d.Drug_Ingredient.Where(di => di.ingredient_id == d.id);

                List<DrugSmallViewModel> drug = new List<DrugSmallViewModel>();
                foreach (var i in data)
                {
                    drug.Add(new DrugSmallViewModel (i.Drug, db.DrugDatas.First(dd => dd.id == i.drug_id).description));
                }
                da.Add(new IngredientViewModel(d, new IngredientDataViewModel(d.IngredientData), drug));
            }

            return da;
        }

        public IQueryable<IngredientData> GetIngredientDatas()
        {
            return db.IngredientDatas;
        }

        public IngredientViewModel GetWholeData(int id)
        {
            var ingredient = db.Ingredients.Find(id);
            if (ingredient == null)
                return new IngredientViewModel();

            List<IngredientViewModel> da = new List<IngredientViewModel>();
            var data = ingredient.Drug_Ingredient.Where(di => di.ingredient_id == ingredient.id);

            List<DrugSmallViewModel> drug = new List<DrugSmallViewModel>();
            foreach (var i in data)
            {
                drug.Add(new DrugSmallViewModel(i.Drug, db.DrugDatas.First(dd => dd.id == i.drug_id).description));
            }

            return new IngredientViewModel(ingredient, new IngredientDataViewModel(ingredient.IngredientData), drug);
        }

        public IEnumerable<IngredientSmallViewModel> GetSmallIngredients()
        {
            var ingredients = db.Ingredients;

            List<IngredientSmallViewModel> ings = new List<IngredientSmallViewModel>();
            foreach (var i in ingredients)
            {
                ings.Add( new IngredientSmallViewModel(i, i.IngredientData.effects ?? i.IngredientData.other));
            }

            return ings;
        }

        public bool Add(Ingredient ingredient, IngredientData data)
        {

            db.IngredientDatas.Add(data);
            db.SaveChanges();

            var id = db.IngredientDatas.OrderByDescending(ing => ing.id).Take(1);
            ingredient.data_id = id.First().id;
            db.Ingredients.Add(ingredient);
            db.SaveChanges();

            return true;
        }

        public bool Update(Ingredient ingredient, IngredientData data)
        {
            db.Entry(ingredient).State = System.Data.Entity.EntityState.Modified;
            db.Entry(data).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return true;
        }

        public IngredientData GetData(int id)
        {
            return db.IngredientDatas.FirstOrDefault(d => d.id == id);
        }
    }
}