﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace BazaLekow_BAS.Models
{
    public interface IBLDbContext : IDisposable
    {
        IDbSet<Drug> Drugs { get; set; }
        IDbSet<DrugProduct> DrugProducts { get; set; }
        IDbSet<Ingredient> Ingredients { get; set; }
        IDbSet<IngredientData> IngredientDatas { get; set; }
        IDbSet<Drug_Ingredient> Drug_Ingredient { get; set; }
        IDbSet<DrugData> DrugDatas { get; set; }
        IDbSet<Admin> Admins { get; set; }

        int SaveChanges();
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
    }
}