﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BazaLekow_BAS.Models.ViewModel
{
    public class DrugDataViewModel
    {
        public DrugDataViewModel()
        {

        }

        public DrugDataViewModel(DrugData dd)
        {
            Id = dd.id;
            Description = dd.description;
            Ingredients_effect = dd.ingredients_effects;
            Use = dd.use;
            Dont_use = dd.dont_use;
            Caution = dd.caution;
            Dosage = dd.dosage;
            Pregnant = dd.pregnant;
            Interactions = dd.interactions;
            Side_effects = dd.side_effects;
            Other = dd.other;
        }
        public DrugData ToDrugData()
        {
            DrugData dd = new DrugData();
            dd.id = Id;
            dd.description = Description;
            dd.ingredients_effects = Ingredients_effect;
            dd.use = Use;
            dd.dont_use = Dont_use;
            dd.caution = Caution;
            dd.dosage = Dosage;
            dd.pregnant = Pregnant;
            dd.interactions = Interactions;
            dd.side_effects = Side_effects;
            dd.other = Other;

            return dd;
        }
        public void OverrideValues(DrugData dd)
        {
            dd.description = Description;
            dd.ingredients_effects = Ingredients_effect;
            dd.use = Use;
            dd.dont_use = Dont_use;
            dd.caution = Caution;
            dd.dosage = Dosage;
            dd.pregnant = Pregnant;
            dd.interactions = Interactions;
            dd.side_effects = Side_effects;
            dd.other = Other;
        }

        public int Id { get; set; }
        public string Description { get; set; }
        public string Ingredients_effect { get; set; }
        public string Use { get; set; }
        public string Dont_use { get; set; }
        public string Caution { get; set; }
        public string Dosage { get; set; }
        public string Pregnant { get; set; }
        public string Interactions { get; set; }
        public string Side_effects { get; set; }
        public string Other { get; set; }
    }
}