/// <autosync enabled="true" />
/// <reference path="angular.min.js" />
/// <reference path="angular-animate.min.js" />
/// <reference path="angular-aria.min.js" />
/// <reference path="angularcontrollers/admins.js" />
/// <reference path="angularcontrollers/drugs.js" />
/// <reference path="angularcontrollers/drugsadmin.js" />
/// <reference path="angularcontrollers/ingredients.js" />
/// <reference path="angularcontrollers/ingredientsadmin.js" />
/// <reference path="angularcontrollers/login.js" />
/// <reference path="angularcontrollers/routing.js" />
/// <reference path="angular-cookies.min.js" />
/// <reference path="angular-loader.min.js" />
/// <reference path="angular-message-format.min.js" />
/// <reference path="angular-messages.min.js" />
/// <reference path="angular-mocks.js" />
/// <reference path="angular-parse-ext.min.js" />
/// <reference path="angular-resource.min.js" />
/// <reference path="angular-route.min.js" />
/// <reference path="angular-sanitize.min.js" />
/// <reference path="angular-touch.min.js" />
/// <reference path="angular-ui/ui-bootstrap.min.js" />
/// <reference path="angular-ui/ui-bootstrap-tpls.min.js" />
/// <reference path="autoheight.js" />
/// <reference path="bootstrap.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-1.10.2.js" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="myapp.js" />
/// <reference path="respond.js" />
