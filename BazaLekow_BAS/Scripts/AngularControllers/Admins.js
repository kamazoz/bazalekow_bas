﻿angular.module('MyApp')
.controller('AdminController', ['$window', '$rootScope', '$scope', 'AdminService', function ($window, $rootScope, $scope, AdminService) {

    var vm = this;
    $scope.Admin = {};

    _init(window.MvcModel);

    function _init(model) {
        vm.model = model;
    }

    applyNewModel = function () {
        AdminService.getAdmins().then(function (response) {
            vm.model = response.data;
        });
    }

    $scope.openModal = function () {
        $('#viewModal').modal('show');
        $scope.Admin.Username = '';
        $scope.Admin.Password = '';
        $scope.isEdit = false;
    }

    $scope.closeModal = function () {
        $('#viewModal').modal('hide');
    }

    $scope.addAdmin = function () {
        $scope.empty = true;
        $scope.sameName = null;
        if (this.Admin.Username) {
            vm.model.forEach(function find(item) {
                if (item.Username == $scope.Admin.Username) {
                    $scope.sameName = "Istnieje taka nazwa";
                }
            });
        }
        if (this.Admin.Username && this.Admin.Password && !$scope.sameName) {
            $scope.empty = false;
            $scope.sameName = null;
            $('#viewModal').modal('hide');
            AdminService.addAdmin(this.Admin).then(function (a) {
                applyNewModel();
            });
        }
    }

    $scope.deleteAdmin = function () {
        AdminService.deleteAdmin(this.d).then(function (d) {
            applyNewModel();
        });
    }

    $scope.editAdmin = function () {
        AdminService.editAdmin(this.Admin).then(function (d) {
            applyNewModel();
        });
        $('#viewModal').modal('hide');
    }

    $scope.detailsAdmin = function () {
        AdminService.detailsAdmin(this.d).then(function (d) {
            $scope.Admin.Id = d.data.Id;
            $scope.Admin.Username = d.data.Username;
        });

        $scope.isEdit = true;
        $scope.Admin.Password = '';
        $('#viewModal').modal('show');
    }

    /*getAdmins();
    function getAdmins() {
        AdminService.getAdmins()
            .then(function success(response) {
                $scope.admins = response;
                console.log($scope.response);
            }, function error(response) {
                $scope.status = 'Unable to load admins data: ' + error.response;
                console.log($scope.status);
            });
    }*/
}])
.factory('AdminService', ['$http', function ($http) {
    var AdminService = {};
    AdminService.getAdmins = function () {
        return $http.get('/Admins/IndexJSON');
    };

    AdminService.addAdmin = function (admin) {
        return $http.post('/Admins/CreateJSON', admin, {params:{'Username': admin.Username, 'Password': admin.Password} });
    };

    AdminService.deleteAdmin = function (admin) {
        return $http.post('/Admins/DeleteJSON', admin, { params: { 'id': admin.Id } });
    };

    AdminService.detailsAdmin = function (admin) {
        return $http.get('/Admins/DetailsJSON', { params: { 'id': admin.Id } });
    };

    AdminService.editAdmin = function (admin) {
        return $http.post('/Admins/EditJSON', admin, { params: { 'Id': admin.Id, 'Username': admin.Username, 'Password': admin.Password } });
    };
    return AdminService;
}]);
