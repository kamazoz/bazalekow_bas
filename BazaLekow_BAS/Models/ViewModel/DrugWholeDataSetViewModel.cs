﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BazaLekow_BAS.Models.ViewModel
{
    public class DrugWholeDataSetViewModel
    {
        public int Id{ get; set; }
        public string Name { get; set; }
        public DrugDataViewModel Data { get; set; }
        public List<DrugProductViewModel> Products{ get; set; }
        public List<IngredientSmallViewModel> Ingredients { get; set; }
    }
}