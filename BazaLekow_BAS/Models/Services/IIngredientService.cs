﻿using BazaLekow_BAS.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace BazaLekow_BAS.Models.Services
{
    public interface IIngredientService : IService<Ingredient>
    {
        IEnumerable<IngredientViewModel> GetAllWithDrugs();
        IQueryable<IngredientData> GetIngredientDatas();
        IngredientViewModel GetWholeData(int id);
        IEnumerable<IngredientSmallViewModel> GetSmallIngredients();
        bool Add(Ingredient ingredient, IngredientData data);
        bool Update(Ingredient ingredient, IngredientData data);
        IngredientData GetData(int id);
    }
}
