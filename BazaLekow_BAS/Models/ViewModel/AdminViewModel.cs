﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BazaLekow_BAS.Models.ViewModel
{
    public class AdminViewModel
    {
        public AdminViewModel()
        {
        }
        public AdminViewModel(Admin admin)
        {
            Id = admin.id;
            Username = admin.username;
            Password = "*****";
        }
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}