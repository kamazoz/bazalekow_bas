﻿/// <reference path="C:\Users\Admin\Documents\Visual Studio 2015\Projects\BazaLekow_BAS\BazaLekow_BAS\Views/Admins/Edit.cshtml" />


angular.module('MyApp')
.controller('DrugsAdminController', ['$window', '$rootScope', '$scope', 'DrugAdminService', 'IngredientAdminService', function ($window, $rootScope, $scope, DrugAdminService, IngredientAdminService) {
    var vm = this;

    $scope.drugData = {};

    applyNewModel = function () {
        DrugAdminService.getDrugs().then(function (response) {
            vm.model = response.data;
        });
    }

    $scope.addProduct = function () {
        if (!$scope.drugData.Products)
            $scope.drugData.Products = [];
        $scope.drugData.Products.push({});
    }

    $scope.removeProduct = function () {
        $scope.Drug.Products[this.$index] = null;
        $scope.drugData.Products.splice(this.$index, 1);

    }

    $scope.addIngredient = function () {
        if (!$scope.drugData.Ingredients)
            $scope.drugData.Ingredients = [];

        $scope.drugData.Ingredients.push({});
    }

    $scope.removeIngredient = function () {
        $scope.Drug.Ingredients[this.$index] = null;
        $scope.drugData.Ingredients.splice(this.$index, 1);

    }

    $scope.detailsDrug = function () {
        $scope.isEdit = true;
        DrugAdminService.detailsDrug(this.d).then(function (d) {
            $scope.drugData = d.data;
            $scope.Drug = d.data;
            $scope.Drug.Id = d.data.Id;
        });
        IngredientAdminService.getIngredients().then(function (response) {
            vm.Ingredients = response.data;
        });
        $('#viewModal').modal('show');
    }

    $scope.addDrug = function () {
        DrugAdminService.addDrug({ Drug: $scope.Drug}).then(function (d) {
            applyNewModel();
        });

        if($scope.Drug.Name)
            $('#viewModal').modal('hide');
    }

    $scope.deleteDrug = function () {
        DrugAdminService.deleteDrug(this.d).then(function (d) {
            applyNewModel();
        });
    }

    $scope.editDrug = function () {
        DrugAdminService.editDrug({ Drug: $scope.Drug }).then(function (d) {
            applyNewModel();
        });
        
        $('#viewModal').modal('hide');
    }

    $scope.openModal = function () {
        $scope.Drug = {};
        $scope.Drug.Products = {};
        $scope.Drug.Ingredients = {};
        $scope.drugData = {};
        $scope.drugData.Name = "'Nowy Lek'";
        $scope.isEdit = false;
        IngredientAdminService.getIngredients().then(function (response) {
            vm.Ingredients = response.data;
        });
        $('#viewModal').modal('show');
    }

    $scope.closeModal = function () {
        $('#viewModal').modal('hide');
    }

    _init(window.MvcModel);

    function _init(model) {
        vm.model = model;
    }
}])
.factory('DrugAdminService', ['$http', function ($http) {
    var DrugAdminService = {};
    DrugAdminService.getDrugs = function () {
        return $http.get('/Drugs/IndexJSON');
    };

    DrugAdminService.addDrug = function (drug) {
        return $http.post('/Drugs/CreateJSON', 
            {
                data: drug.Drug.Data,
                name: drug.Drug.Name,
                products: drug.Drug.Products ? Array.from(Object.keys(drug.Drug.Products), k=>drug.Drug.Products[k]) : drug.Drug.Products,
                ingredients: drug.Drug.Ingredients ? Array.from(Object.keys(drug.Drug.Ingredients), k=>drug.Drug.Ingredients[k]) : drug.Drug.Ingredients
            });
    };

    DrugAdminService.deleteDrug = function (drug) {
        return $http.post('/Drugs/DeleteJSON', drug, { params: { 'id': drug.Id } });
    };

    DrugAdminService.detailsDrug = function (drug) {
        return $http.get('/Drugs/DetailsGuestJSON', { params: { 'id': drug.Id } });
    };

    DrugAdminService.editDrug = function (drug) {
        return $http.post('/Drugs/EditJSON',
                {
                    id: drug.Drug.Id,
                    data: drug.Drug.Data,
                    name: drug.Drug.Name,
                    products: drug.Drug.Products ? Array.from(Object.keys(drug.Drug.Products), k=>drug.Drug.Products[k]) : drug.Drug.Products,
                    ingredients: drug.Drug.Ingredients ? Array.from(Object.keys(drug.Drug.Ingredients), k=>drug.Drug.Ingredients[k]) : drug.Drug.Ingredients
                });
    };
    return DrugAdminService;
}]);
