﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BazaLekow_BAS.Models.ViewModel
{
    public class IngredientDataViewModel
    {
        public IngredientDataViewModel()
        {

        }
        public IngredientDataViewModel(IngredientData id)
        {
            Id = id.id;
            Effects = id.effects;
            Indications = id.indications;
            Contradictions = id.contradictions;
            Interactions = id.interactions;
            Side_effects = id.side_effects;
            Pregnancy = id.pregnancy;
            Dosage = id.dosage;
            Comments = id.comments;
            Other = id.other;
        }
        public IngredientData ToIngredientData()
        {
            IngredientData id = new IngredientData();

            //id.id = Id;
            id.effects = Effects;
            id.indications = Indications;
            id.contradictions = Contradictions;
            id.interactions = Interactions;
            id.side_effects = Side_effects;
            id.pregnancy = Pregnancy;
            id.dosage = Dosage;
            id.comments = Comments;
            id.other = Other;

            return id;
        }

        public int Id { get; set; }
        public string Effects { get; set; }
        public string Indications { get; set; }
        public string Contradictions { get; set; }
        public string Interactions { get; set; }
        public string Side_effects { get; set; }
        public string Pregnancy { get; set; }
        public string Dosage { get; set; }
        public string Comments { get; set; }
        public string Other { get; set; }
    }
}