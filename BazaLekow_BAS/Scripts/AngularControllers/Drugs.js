﻿/// <reference path="C:\Users\Admin\Documents\Visual Studio 2015\Projects\BazaLekow_BAS\BazaLekow_BAS\Views/Admins/Edit.cshtml" />
angular.module('MyApp')
.controller('DrugsController', ['$window', '$rootScope', '$scope', 'DrugService', function ($window, $rootScope, $scope, DrugService) {
    var vm = this;

    $scope.getData = function () {
        DrugService.getWholeData(this.d.Id).then(function (d) {
            $scope.drugData = d.data;
        });
        $('#viewModal').modal('show');

    }

    $scope.getTmpData = function () {
        DrugService.getWholeData(this.a.Id).then(function (a) {
            $scope.drugData = a.data;
        });

        $('#viewModal').modal('show');
    }

    $scope.closeModal = function () {
        $('#viewModal').modal('hide').scrollTop(0);
    }

    $scope.createIngredientsData = function () {
        //function getAllIngredients(item) {
        //    var ingredients = [];
        //    item.forEach(function (Ingredient) {
        //        ingredients.push(Ingredient);
        //    });

        //    return ingredients;
        //}
        $window.sessionStorage.setItem("ingredients", JSON.stringify(this.drugData.Ingredients));;
        var url = $window.location.host + '/Ingredients/Guest';
        $window.location.href = 'http://' + url;
    }
    $scope.createDrugsData = function () {
        //function getAllSimilarDrugs(item) {
        //    var drugs = [];
        //    item.forEach(function (Ingredient) {
        //        vm.model.forEach(function (Drug) {
        //            Drug.Ingredients.forEach(function (ing) {
        //                if (ing.Id == Ingredient.Id)
        //                    drugs.push(Drug);
        //            })
        //        })
        //    });

        //    return drugs;
        //}
        vm.tmp = this.drugData.Drugs;
        $('#viewModal').modal('hide');
    }

    $scope.removeTmpData = function () {
        vm.tmp = null;
        $window.sessionStorage.setItem("drugs", null);
    }

    _init(window.MvcModel);

    function _init(model) {
        vm.model = model;
        vm.tmp = $.parseJSON($window.sessionStorage.getItem("drugs"));
    }
}])
.factory('DrugService', ['$http', function ($http) {
    var DrugService = {};

    DrugService.getWholeData = function (id) {
        return $http({
            url: '/Drugs/DetailsGuestJSON',
            params: { 'id': id },
            method: 'GET',
            data: JSON.stringify(id),
            headers: { 'content-type': 'application/json' }
        });
    };

    return DrugService;
}]);
