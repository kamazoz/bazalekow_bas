﻿angular.module('MyApp',['ngRoute'])
.config(function ($routeProvider, $locationProvider) {
    $routeProvider
    .when('/', {
        redirectTo: function () {
            return '/drugs';
        }
    })
    //.when('/home', {
    //    templateUrl: '/Home/Index',
    //    controller: 'HomeController'
    //})
    .when('/drugs', {
        templateUrl: '/Drugs/Guest',
        controller: 'DrugsController'
    })
    .when('/ingredients', {
        templateUrl: '/Ingredients/Guest',
        controller: 'IngredientsController'
    })
    .when('/login', {
        templateUrl: '/Login/Index'
    })
    .otherwise({
        templateUrl: '/Shared/Error',
        controller: 'ErrorController'
    })

    $locationProvider.html5Mode(false).hashPrefix('!');
})
.controller('HomeController', function ($scope) {

})
.controller('AdminsController', function ($scope, $routeParams) {

})
.controller('DrugsController', function ($scope, $routeParams) {

})
.controller('IngredientsController', function ($scope, $routeParams) {

})
.controller('ErrorController', function ($scope, $routeParams) {

})