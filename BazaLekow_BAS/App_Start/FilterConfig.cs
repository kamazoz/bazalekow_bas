﻿using System.Web;
using System.Web.Mvc;

namespace BazaLekow_BAS
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
