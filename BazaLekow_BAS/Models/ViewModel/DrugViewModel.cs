﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BazaLekow_BAS.Models.ViewModel
{
    public class DrugViewModel
    {
        public DrugViewModel()
        {

        }
        public DrugViewModel(Drug d, DrugDataViewModel ddvm, List<IngredientSmallViewModel> i, List<DrugProductViewModel> products, List<DrugSmallViewModel> drugs)
        {
            Id = d.id;
            Name = d.name;
            Data = ddvm;
            Ingredients = i;
            Products = products;
            Drugs = drugs;
        }

        public Drug ToDrug()
        {
            Drug d = new Drug();

            d.id = Id;
            d.name = Name;
            d.data_id = Data.Id;

            return d;
        }

        public int Id { get; set; }
        public string  Name { get; set; }
        public DrugDataViewModel Data { get; set; }
        public List<IngredientSmallViewModel> Ingredients{ get; set; }
        public List<DrugProductViewModel> Products { get; set; }
        public List<DrugSmallViewModel> Drugs { get; set; }
    }
}