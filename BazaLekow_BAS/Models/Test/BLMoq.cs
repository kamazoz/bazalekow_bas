﻿using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BazaLekow_BAS.Models.Test
{
    public class BLMoq
    {
        protected virtual IBLDbContext GetMockContext(
            IList<Drug> Drugs, IList<DrugData> DrugsData, IList<DrugProduct> DrugProducts,
            IList<Ingredient> Ingredients, IList<IngredientData> IngredientsData,
            IList<Drug_Ingredient> Drugs_Ingredients,
            IList<Admin> Admins)
        {
            var dbSetDrugs = GetMockDbSet<Drug>(Drugs);
            var dbSetDrugsData = GetMockDbSet<DrugData>(DrugsData);
            var dbSetDrugProducts = GetMockDbSet<DrugProduct>(DrugProducts);
            var dbSetIngredients = GetMockDbSet<Ingredient>(Ingredients);
            var dbSetIngredientsData = GetMockDbSet<IngredientData>(IngredientsData);
            var dbSetDrugs_Ingredients = GetMockDbSet<Drug_Ingredient>(Drugs_Ingredients);
            var dbSetAdmins = GetMockDbSet<Admin>(Admins);

            var mockContext = new Mock<IBLDbContext>();
            mockContext.Setup(m => m.Drugs).Returns(dbSetDrugs);
            mockContext.Setup(m => m.DrugDatas).Returns(dbSetDrugsData);
            mockContext.Setup(m => m.DrugProducts).Returns(dbSetDrugProducts);
            mockContext.Setup(m => m.Ingredients).Returns(dbSetIngredients);
            mockContext.Setup(m => m.IngredientDatas).Returns(dbSetIngredientsData);
            mockContext.Setup(m => m.Drug_Ingredient).Returns(dbSetDrugs_Ingredients);
            mockContext.Setup(m => m.Admins).Returns(dbSetAdmins);

            return mockContext.Object;
        }

        protected virtual IDbSet<T> GetMockDbSet<T>(IList<T> data) where T : class
        {
            var queryable = data.AsQueryable();
            var mockSet = new Mock<IDbSet<T>>();
            mockSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(queryable.Provider);
            mockSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(queryable.Expression);
            mockSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(queryable.ElementType);
            mockSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(queryable.GetEnumerator());
            mockSet.Setup(d => d.Add(It.IsAny<T>())).Callback<T>((s) => data.Add(s));

            return mockSet.Object;
        }
    }
}