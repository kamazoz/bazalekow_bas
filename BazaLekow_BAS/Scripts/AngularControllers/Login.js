﻿angular.module("MyApp")
.controller('LoginController', ['$window', '$scope', 'LoginService', function ($window, $scope, LoginService) {
    $scope.IsLogedIn = false;
    $scope.Message = '';
    $scope.Submitted = false;
    $scope.IsFormValid = false;

    $scope.LoginData = {
        Username: '',
        Password: ''
    };

    $scope.showModal = function () {
        $('#loginModal').modal('show');
        $('#loginModal').find("input").val('').end();
    }

    $scope.closeModal = function () {
        $('#loginModal').modal('hide');
    }

    $scope.$watch('f1.$valid', function (newVal) {
        $scope.IsFormValid = newVal;
    });

    $scope.Login = function () {
        $scope.Submitted = true;
        $scope.Message = null;

        if ($scope.IsFormValid) {
            LoginService.Login($scope.LoginData).then(function (d) {
                if (d.data.username != null) {
                    $scope.IsLogedIn = true;
                    $window.location.reload();
                }
                else {
                    $scope.Submitted = true;
                    $scope.Message = "Nieznany użytkownik lub złe hasło.";
                    $('#loginModal').find("input").val('').end();
                }
            })
        }
    };

    $scope.Logout = function () {
        LoginService.Logout().then(function () {
                $window.location.href = "http://" + $window.location.host;
        })
    };
}])
.factory('LoginService', function ($http) {
    var fac = {};
    fac.Login = function (d) {
        return $http({
            url: '/Login/Index',
            method: 'POST',
            data: JSON.stringify(d),
            headers: { 'content-type': 'application/json' }
        });
    };
    fac.Logout = function () {
        return $http({
            url: '/Login/Logout',
            method: 'GET',
            headers: { 'content-type': 'application/json' }
        });
    };
    return fac;
});