﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BazaLekow_BAS.Models.ViewModel;
using BazaLekow_BAS.Models;
using BazaLekow_BAS.Utility;
using BazaLekow_BAS.Models.Services;

namespace BazaLekow_BAS.Controllers
{
    [AuthorizationFilter]
    public class AdminsController : Controller
    {
        private IAdminService service;

        public AdminsController(IAdminService service)
        {
            this.service = service;
        }

        // GET: Admins
        public ActionResult Index()
        {
            var admins = service.GetAll().ToList();
            List<AdminViewModel> aView = new List<AdminViewModel>(admins.Count);
            for(int i = 0; i < admins.Count; ++i)
            {
                aView.Add(new AdminViewModel(admins[i]));
            }

            return View(aView);
        }

        // GET: Admins
        public JsonResult IndexJSON()
        {
            var admins = service.GetAll().ToList();
            List<AdminViewModel> aView = new List<AdminViewModel>(admins.Count);
            for (int i = 0; i < admins.Count; ++i)
            {
                aView.Add(new AdminViewModel(admins[i]));
            }

            return Json(aView, JsonRequestBehavior.AllowGet);
        }

        // GET: Admins/Details/5
        [HttpGet]
        public JsonResult DetailsJSON(int? id)
        {
            if (id == null)
            {
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            Admin admin = service.Get(id.Value);
            AdminViewModel a = new AdminViewModel(admin);
            if (admin == null)
            {
                return Json("Błąd", JsonRequestBehavior.AllowGet);
            }

            return Json(a, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [ValidateAntiForgeryHeader]
        public JsonResult CreateJSON([Bind(Include = "Username,Password")] AdminViewModel admin)
        {
            if (ModelState.IsValid)
            {
                Admin a = new Admin();
                a.username = admin.Username;
                a.password = Utility.Utility.hashPassword(admin.Password);
                service.Add(a);
                return Json("Success", JsonRequestBehavior.AllowGet);
            }

            return Json("Błąd", JsonRequestBehavior.AllowGet);
        }
  



        [HttpPost]
        [ValidateAntiForgeryHeader]
        public JsonResult EditJSON([Bind(Include = "Id,Username,Password")] AdminViewModel admin)
        {
            if(admin == null && admin.Username == "" || admin.Id == 0)
                return Json("Błąd", JsonRequestBehavior.AllowGet);

            if (ModelState.IsValid)
            {
                Admin old = service.Get(admin.Id);
                if(old.username != admin.Username)
                    old.username = admin.Username;
                if (admin.Password != null)
                    old.password = Utility.Utility.hashPassword(admin.Password);

                service.Update(old);
                return Json("Success", JsonRequestBehavior.AllowGet);
            }

            return Json("Błąd", JsonRequestBehavior.AllowGet);
        }



        // POST: Admins/Delete/5
        [HttpPost]
        [ValidateAntiForgeryHeader]
        public JsonResult DeleteJSON(int? id)
        {
            if(id == null)
                return Json("Błąd", JsonRequestBehavior.AllowGet);

            service.Delete(id.Value);
            return Json("Success", JsonRequestBehavior.AllowGet);
        }
    }
}
