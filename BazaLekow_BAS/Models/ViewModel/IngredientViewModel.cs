﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BazaLekow_BAS.Models.ViewModel
{
    public class IngredientViewModel
    {
        public IngredientViewModel()
        {

        }
        public IngredientViewModel(Ingredient i, IngredientDataViewModel idvm, List<DrugSmallViewModel> d)
        {
            Id = i.id;
            Name = i.name;
            Data = idvm;
            Drugs = d;
        }

        public Ingredient ToIngredient()
        {
            Ingredient i = new Ingredient();

            i.id = Id;
            i.name = Name;
            i.data_id = Data.Id;

            return i;
        }

        public int Id { get; set; }
        public string Name{ get; set; }
        public IngredientDataViewModel Data{ get; set; }
        public List<DrugSmallViewModel> Drugs { get; set; }
    }
}