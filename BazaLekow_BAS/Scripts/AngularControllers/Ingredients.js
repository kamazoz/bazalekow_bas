﻿angular.module('MyApp')
.controller('IngredientsController', ['$window', '$rootScope', '$scope', 'IngredientService', function ($window, $rootScope, $scope, IngredientService) {
    var vm = this;

    $scope.getData = function () {
        IngredientService.getWholeData(this.d.Id).then(function (d) {
            $scope.drugData = d.data;
        });
        $('#viewModal').modal('show');
    }

    $scope.getTmpData = function () {
        IngredientService.getWholeData(this.a.Id).then(function (a) {
            $scope.drugData = a.data;
        });
        $('#viewModal').modal('show');
    }

    $scope.closeModal = function () {
        $('#viewModal').modal('hide');
    }

    //$scope.createTmpData = function () {
    //    function getAllSimilarDrugs(item) {
    //        var drugs = [];
    //        item.forEach(function (Drug) {
    //            drugs.push(Drug);
    //        });

    //        return drugs;
    //    }
    //    $window.sessionStorage.setItem("drugs", JSON.stringify(getAllSimilarDrugs(this.drugData.Drugs)));

    //    var url = 'http://' + $window.location.host + '/Drugs/Guest';
    //    $window.location.href = url;
    //}

    $scope.createDrugsData = function () {
        //function getAllSimilarDrugs(item) {
        //    var drugs = [];
        //    item.forEach(function (Ingredient) {
        //        vm.model.forEach(function (Drug) {
        //            Drug.Ingredients.forEach(function (ing) {
        //                if (ing.Id == Ingredient.Id)
        //                    drugs.push(Drug);
        //            })
        //        })
        //    });

        //    return drugs;
        //}
        $window.sessionStorage.setItem("drugs", JSON.stringify(this.drugData.Drugs));;
        var url = $window.location.host + '/Drugs/Guest';
        $window.location.href = "http://" + url;
    }

    $scope.removeTmpData = function () {
        vm.tmp = null;
        $window.sessionStorage.setItem("ingredients", null);
    }

    _init(window.MvcModel);

    function _init(model) {
        vm.model = model;
        vm.tmp  = $.parseJSON($window.sessionStorage.getItem("ingredients"));
    }
}])
.factory('IngredientService', ['$http', function ($http) {
    var IngredientService = {};

    IngredientService.getWholeData = function (id) {
        return $http({
            url: '/Ingredients/DetailsGuestJSON',
            params: { 'id': id },
            method: 'GET',
            data: JSON.stringify(id),
            headers: { 'content-type': 'application/json' }
        });
    };

    return IngredientService;
}]);