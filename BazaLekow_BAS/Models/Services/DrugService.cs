﻿using BazaLekow_BAS.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BazaLekow_BAS.Models.Services
{
    public class DrugService : IDrugService
    {
        private BLEntities db;

        public DrugService(BLEntities db)
        {
            this.db = db;
        }

        private void ezAdd(DrugWholeDataSetViewModel drug, int id)
        { 

            if (drug.Products != null)
            {
                foreach (var product in drug.Products)
                {
                    if (product == null)
                        continue;

                    product.Drug_id = id;
                    db.DrugProducts.Add(product.ToDrugProduct());
                }
            }

            if (drug.Ingredients != null)
            {
                var ingredients = db.Ingredients.ToArray();

                foreach (var ingredient in drug.Ingredients)
                {
                    if (ingredient == null)
                        continue;
                    var i = ingredients.Where(get => get.name == ingredient.Name).FirstOrDefault();
                    if (i != null)
                    {
                        Drug_Ingredient dI = new Drug_Ingredient();
                        dI.drug_id = id;
                        dI.ingredient_id = i.id;
                        db.Drug_Ingredient.Add(dI);
                    }
                }
            }
        }

        public bool Add(DrugWholeDataSetViewModel drug)
        {
            DrugData dd = new DrugData();

            if(drug.Data != null)
                dd = drug.Data.ToDrugData();

            db.DrugDatas.Add(dd);
            db.SaveChanges();

            int lastData = db.DrugDatas.OrderByDescending(d => d.id).Take(1).First().id;
            Drug newDrug = new Drug();
            newDrug.name = drug.Name;
            newDrug.data_id = lastData;
            db.Drugs.Add(newDrug);
            db.SaveChanges();

            int lastDrug = db.Drugs.OrderByDescending(d => d.id).Take(1).First().id;
            ezAdd(drug, lastDrug);
            db.SaveChanges();

            return true;
        }

        public bool Add(Drug item)
        {
            db.Drugs.Add(item);
            db.SaveChanges();
            return true;
        }

        private void ezDelete(int id)
        {
            var diToRemove = db.Drug_Ingredient.Where(drug_ing => drug_ing.drug_id == id).ToArray();
            if (diToRemove != null)
            {
                foreach (var di in diToRemove)
                {
                    db.Drug_Ingredient.Remove(di);
                }
            }

            var dpToRemove = db.DrugProducts.Where(drug_ing => drug_ing.drug_id == id).ToArray();
            if (dpToRemove != null)
            {
                foreach (var dp in dpToRemove)
                {
                    db.DrugProducts.Remove(dp);
                }
            }
        }

        public bool Delete(int id)
        {
            ezDelete(id);
            var drug = db.Drugs.FirstOrDefault(d => d.id == id);
            var drugData = db.DrugDatas.First(toRemove => toRemove.id == drug.data_id);
            if (drugData != null)
                db.DrugDatas.Remove(drugData);

            db.Drugs.Remove(db.Drugs.First(d => d.id == id));
            db.SaveChanges();
            return true;
        }

        public Drug Get(int id)
        {
            return db.Drugs.FirstOrDefault(d => d.id == id);
        }

        public IQueryable<Drug> GetAll()
        {
            db.Configuration.LazyLoadingEnabled = false;

            return db.Drugs;
        }

        public IEnumerable<DrugSmallViewModel> GetSmallDrugs()
        {
            var drugs = db.Drugs;
            if (drugs == null)
                return new List<DrugSmallViewModel>();

            List<DrugSmallViewModel> dsvm = new List<DrugSmallViewModel>();
            foreach(var d in drugs.ToArray() ?? new Drug[0])
            {
                dsvm.Add(new DrugSmallViewModel(d, d.DrugData.description));
            }

            return dsvm;
        }

        public DrugViewModel GetWholeData(int id)
        {
            var drug = db.Drugs.Find(id);
            if (drug == null)
                return new DrugViewModel();

            var data = drug.Drug_Ingredient.Where(di => di.drug_id == drug.id);

            List<IngredientSmallViewModel> ing = new List<IngredientSmallViewModel>();
            foreach (var i in data)
            {
                ing.Add(new IngredientSmallViewModel(i.Ingredient));
            }
            List<DrugProductViewModel> dp = new List<DrugProductViewModel>();
            foreach (var i in drug.DrugProducts)
            {
                dp.Add(new DrugProductViewModel(i));
            }
            List<DrugSmallViewModel> drugs = new List<DrugSmallViewModel>();

            HashSet<int> keys = new HashSet<int>();
            foreach (var i in data)
            {
                var t = db.Drug_Ingredient.Where(di => di.ingredient_id == i.ingredient_id).ToList();

                foreach(var dr in t)
                {
                    if (keys.Add(dr.drug_id))
                    {
                        drugs.Add(new DrugSmallViewModel(dr.Drug, dr.Drug.DrugData.description));
                    }

                }
            }

            return new DrugViewModel(drug, new DrugDataViewModel(drug.DrugData), ing, dp, drugs);
        }

        public bool Update(DrugWholeDataSetViewModel drug)
        {
            //DrugData dd = new DrugData();

            //if (drug.Data != null)
            //    dd = drug.Data.ToDrugData();

            //db.DrugDatas.Add(dd);
            //db.SaveChanges();

            //int lastData = db.DrugDatas.OrderByDescending(d => d.id).Take(1).First().id;
            //Drug newDrug = new Drug();
            //newDrug.name = drug.Name;
            //newDrug.data_id = lastData;
            //db.Drugs.Add(newDrug);
            //db.SaveChanges();

            //ezAdd(drug);

            Drug dr = db.Drugs.FirstOrDefault(d => d.id == drug.Id);
            dr.name = drug.Name;

            DrugData dd = db.DrugDatas.FirstOrDefault(drda => drda.id == dr.data_id);
            drug.Data.OverrideValues(dd);

            ezDelete(drug.Id);
            ezAdd(drug, drug.Id);

            db.Entry(dr).State = System.Data.Entity.EntityState.Modified;
            db.Entry(dd).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return true;
        }

        public bool Update(Drug item)
        {
            db.Entry(item).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            return true;
        }
    }
}