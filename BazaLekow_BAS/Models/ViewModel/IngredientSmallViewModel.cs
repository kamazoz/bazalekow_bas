﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BazaLekow_BAS.Models.ViewModel
{
    public class IngredientSmallViewModel
    {
        public IngredientSmallViewModel()
        {

        }
        public IngredientSmallViewModel(Ingredient i)
        {
            Id = i.id;
            Name = i.name;
        }

        public IngredientSmallViewModel(Ingredient i, string description) : this(i)
        {
            Description = description;
        }

        public Ingredient ToIngredient()
        {
            Ingredient ing = new Ingredient();
            ing.name = this.Name;

            return ing;
        }

        public int Id{ get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}