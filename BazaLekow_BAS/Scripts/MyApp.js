﻿(function () {
    var app = angular.module('MyApp', ['ui.bootstrap']);
    app.run(['$rootScope', '$http', function ($rootScope, $http) {
        $rootScope.RootUrl = window.RootUrl;
        //$http.defaults.headers.common['X-XSRF-Token'] =
        //    angular.element('input[name="__RequestVerificationToken"]').attr('value');
        var antiForgeryToken = document.getElementById('antiForgeryForm').childNodes[1].value;
        $http.defaults.headers.post['__RequestVerificationToken'] = antiForgeryToken;
    }]);
})();

