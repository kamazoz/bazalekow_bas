﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BazaLekow_BAS.Models.Services
{
    public interface IService<T> where T : class
    {
        IQueryable<T> GetAll();
        T Get(int id);
        bool Add(T item);
        bool Update(T item);
        bool Delete(int id);
    }
}